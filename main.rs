use std::f64::consts::PI;
use std::fs::File;
use std::io::prelude::*;

// `std::f64::consts::TAU` is unstable T_T
const TAU: f64 = 2.0 * PI;

struct Stream<'a, T> {
    sample_rate: f64,
    generator: Box<dyn FnMut(usize, f64) -> T>,
    effects: Vec<Effect<T>>,
    combinators: Vec<Combination<'a, T>>,
    index: usize
}
struct Combination<'a, T>(&'a mut Stream<'a, T>, Combinator<T>);
type Effect<T> = Box<dyn Fn(T) -> T>;
type Combinator<T> = Box<dyn Fn(T, T) -> T>;

impl<'a, T> Stream<'a, T> {
    pub fn new(f: impl FnMut(usize, f64) -> T + 'static) -> Self {
        Self {
            sample_rate: 44100.0,
            generator: Box::new(f),
            effects: vec![],
            combinators: vec![],
            index: 0
        }
    }

    pub fn effect(&mut self, f: impl Fn(T) -> T + 'static) -> &mut Self {
        self.effects.push(Box::new(f));
        self
    }

    pub fn combine(&mut self, s: &'a mut Stream<'a, T>, f: impl Fn(T, T) -> T + 'static) -> &mut Self {
        self.combinators.push(Combination(s, Box::new(f)));
        self
    }

    fn get_value(&mut self, idx: usize) -> T {
        let mut val = (self.generator)(idx, (idx as f64) / self.sample_rate);
        for Combination(ref mut target, ref combinator) in self.combinators.iter_mut() {
            val = (combinator)(val, target.get_value(idx))
        }
        for effect in self.effects.iter() {
            val = (effect)(val);
        }
        val       
    }
}

impl<'a, T> Iterator for Stream<'a, T> {
    type Item = T;
    fn next(&mut self) -> Option<T> {
        let ret = self.get_value(self.index);
        self.index += 1;
        Some(ret)
    }
}

fn main() -> std::io::Result<()> {
    let m = Stream::new(|_, f| (TAU * f * 440.0).sin());
    let stream = m.take(44100).collect::<Vec<_>>();
    let mut file = File::create("sine.raw")?;
    let byte_stream = unsafe { std::mem::transmute::<&[f64], &[u8]>(&stream[..]) };
    file.write_all(byte_stream)?;

    Ok(())
}
